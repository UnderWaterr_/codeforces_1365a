package com.company;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int tables = scanner.nextInt();
        for (int i = 0; i < tables; i++) {
            boolean winner = false;
            int column = scanner.nextInt();
            int row = scanner.nextInt();
            int[][] a = new int[column][row];
            for (int j = 0; j < column; j++) {
                for (int k = 0; k < row; k++) {
                    a[j][k] = scanner.nextInt();
                }
            }
            boolean isSuits = true;
            searchForAWinner:
            while (true) {
                searchForAFreeCell:
                for (int columnCheck = 0; columnCheck < column; columnCheck++) {
                    for (int rowCheck = 0; rowCheck < row; rowCheck++) {
                        isSuits = true;
                        if (a[columnCheck][rowCheck] == 0) {

                            for (int izm = 0; izm < row; izm++) {
                                if ((a[columnCheck][izm] == 1) && (izm != rowCheck)) {
                                    isSuits = false;
                                    break;
                                }
                            }
                            for (int changeCell = 0; changeCell < column; changeCell++) {
                                if ((a[changeCell][rowCheck] == 1) && (changeCell != columnCheck)) {
                                    isSuits = false;
                                    break;
                                }
                            }
                        } else {
                            isSuits = !isSuits;
                        }
                        if (isSuits) {
                            a[columnCheck][rowCheck] = 1;
                            break searchForAFreeCell;
                        } else if ((!isSuits) && (columnCheck == column - 1) && (rowCheck == row - 1)) {
                            break searchForAWinner;
                        }
                    }
                }
                if (isSuits) {
                    winner = !winner;
                } else {
                    break;
                }
            }
            if (winner) {
                System.out.println("Ashish");
            } else {
                System.out.println("Vivek ");
            }
        }
    }
}
